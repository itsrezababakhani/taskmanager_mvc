const taskModel = require("../models/taskModel");
const statusModel = require("../models/statusModel");

exports.homeHandler = async (req, res) => {
  if (!("user" in req.session)) {
    res.redirect("/auth/login");
  }
  if (true) {
    const getTasks = await taskModel.getTasks();
    res.render("index", { getTasks, user: req.session.user });
  }
};

exports.addHandler = (req, res) => {
  res.render("add");
};

module.exports.deleteTask = async (req, res) => {
  let id = req.params.task_id;
  const deleteTask = await taskModel.deleteTask(id);
  res.redirect("/");
};

exports.taskAddHandler = async (req, res) => {
  let taskInformation = {
    task_title: req.body.task_title,
    task_category: req.body.task_category,
    task_assignee: req.body.task_owner,
    task_status: statusModel.CREATED
  };
  const addTask = await taskModel.addTask(taskInformation);
  res.redirect("/");
};

exports.editTask = async (req, res) => {
  let id = req.params.id;
  const getTaskById = await taskModel.getTaskById(id);
  console.log(getTaskById);
  res.render("edit", { editItem: getTaskById });
};
