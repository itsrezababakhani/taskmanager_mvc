const authModel = require("../models/authModel");
exports.showLogin = (req, res) => {
  let message = req.session.message || "";
  req.session.message = null;
  res.render("./auth/login", { message });
};

exports.doLogin = async (req, res) => {
  let { email, password } = req.body;
  const checkUserAndPassword = await authModel.checkUserAndPassword(
    email,
    password
  );
  if (checkUserAndPassword.length === 0) {
    req.session.message = "کاربری یافت نشد";
    return res.redirect("/auth/login");
  }
  req.session.user = {
    id: checkUserAndPassword.id,
    first_name: checkUserAndPassword.first_name
  };
  res.redirect("/");
};

exports.showRegister = (req, res) => {
  res.render("./auth/register");
};

exports.doRegister = async (req, res) => {
  let { first_name, last_name, email, password } = req.body;
  let newUserData = {
    first_name,
    last_name,
    email,
    password
  };
  const registerNewUser = await authModel.addNewUser(newUserData);

  if (registerNewUser.affectedRows === 1) {
    req.session.user = {
      id: registerNewUser.insertId,
      first_name: newUserData.first_name
    };
    res.redirect("/");
  }
};

exports.logout = (req, res) => {
  req.session.destroy(() => {
    res.redirect("/auth/login");
  });
};
