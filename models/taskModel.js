const connection = require("../boot/connection");
exports.getTasks = async () => {
  const db = await connection();
  const [tasks, fields] = await db.query("SELECT * FROM `tasks`");
  return tasks;
};

exports.deleteTask = async id => {
  const db = await connection();
  const [result, fields] = await db.query(
    "DELETE FROM `tasks` WHERE `task_id` = ?",
    [id]
  );
  return result;
};

exports.addTask = async taskInformation => {
  const db = await connection();
  const addNewTask = await db.query(
    "INSERT INTO`tasks` SET ?",
    taskInformation
  );
};

exports.getTaskById = async id => {
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM `tasks` WHERE task_id = ?",
    id
  );
  return result[0];
};
