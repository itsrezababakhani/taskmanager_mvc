const connection = require("../boot/connection");

exports.checkUserAndPassword = async (email, password) => {
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM `users` WHERE email=? AND password=? LIMIT 1",
    [email, password]
  );
  return result[0];
};

exports.addNewUser = async newUserData => {
  const db = await connection();
  const [result, fields] = await db.query(
    "INSERT INTO `users` SET ?",
    newUserData
  );
  return result;
};
