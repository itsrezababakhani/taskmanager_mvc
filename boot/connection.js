const mysql2 = require("mysql2/promise");
require("dotenv").config();

const getConnection = async () => {
  const connection = await mysql2.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
  });
  return connection;
};

module.exports = getConnection;
