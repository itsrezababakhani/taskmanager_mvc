const express = require("express");
const app = express();
const router = require("./routes/router");
const middlewareConfig = require("./boot/middlewareConfig");
const appConfig = require("./boot/appConfig");

appConfig(app);
middlewareConfig(app);
router(app);

exports.appStart = () => {
  app.listen(5000, () => {
    console.log("app Work On 5000");
  });
};
