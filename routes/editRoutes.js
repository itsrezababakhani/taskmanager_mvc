const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");
router.get("/:id", taskController.editTask);
module.exports = router;
