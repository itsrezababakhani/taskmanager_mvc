const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

router.get("/", taskController.addHandler);
router.post("/", taskController.taskAddHandler);

module.exports = router;
