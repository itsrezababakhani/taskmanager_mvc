const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");
router.get("/:task_id", taskController.deleteTask);
module.exports = router;
