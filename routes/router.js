const deleteRoutes = require("./deleteRoutes");
const authRoutes = require("./auth");
const homeRoutes = require("./homeRoutes");
const addRoutes = require("./addRoutes");
const editRoutes = require("./editRoutes");

module.exports = app => {
  app.use("/", homeRoutes);
  app.use("/add", addRoutes);
  app.use("/delete", deleteRoutes);
  app.use("/auth", authRoutes);
  app.use("/edit", editRoutes);
};
